resource "aws_instance" "database_instance" {
      ami = "ami-03ec8484ab8a0c807"
      
      # Specifying the Subnet
      subnet_id = aws_subnet.eng89_ray_db_subnet.id
      
      # Provide what type of instance that is being created
      instance_type = var.instance_type

      # Allowing Public IP for the instance
      associate_public_ip_address = true

      # Specifying the SSH Key to Terraform
      key_name = var.aws_key_name

      # Specifying the Security Group
      vpc_security_group_ids = [aws_security_group.eng89_ray_terraform_SG_db.id]

      # Giving a proper name for the instance using tags
      tags = {
            Name = "eng89_ray_terraform_db"
      }
}