# Creating variables for our resources in main.tf to make use of DRY

variable "aws_key_name" {
    default = "eng89_ray_key"
}

# variable "aws_key_path" {
#     default = "~/.ssh/eng89_ray_key.pem"
# }

variable "igw_name" {
    description = "Name of the internet gateway"
    default = "eng89_ray_terraform_IG"
}

variable "cidr_vpc" {
    description = "CIDR block for the VPC"
    default = "10.206.0.0/16"
}

variable "cidr_subnet" {
    description = "CIDR block for the app subnet"
    default = "10.206.1.0/24"
}

variable "cidr_subnet_db" {
    description = "CIDR block for the database subnet"
    default = "10.206.2.0/24"
}

variable "instance_ami" {
    description = "AMI for aws EC2 instance"
    default = "ami-0101ad4503dd3db59"
}

variable "instance_type" {
    description = "type for aws EC2 instance"
    default = "t2.micro"
}

variable "availability_zone" {
    description = "availability zone to create subnet"
    default = "eu-west-1a"
}