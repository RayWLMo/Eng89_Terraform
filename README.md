# IAC With Terraform
Terraform is an infrastructure as code (IaC) tool used to securely and efficiently perform tasks such as build, change and manage infrastructure.

Terraform uses configuration files to define infrastructure components.

Some main benefits of using Terraform:
- Usable with different cloud service providers - Easier migration between platforms
- Lower development costs
- Less time wasted when provisioning
- Infrastructure as code - Allows for easier modification of specifications

Some competitors to Terraform include:
- Ansible
- Vagrant
- Chef

Terraform should be used because it has faster set up and installation compared to Ansible and Vagrant as well as being easier to integrate on a local machine.

![SG and NACL Rules Diagram](https://gitlab.com/RayWLMo/Eng89_Terraform/-/raw/main/images/SG_and_NACL_Rules.png)

![VPC Diagram](https://gitlab.com/RayWLMo/Eng89_Terraform/-/raw/main/images/VPC_Diagram.png)

![Terraform Diagram](https://gitlab.com/RayWLMo/Eng89_Terraform/-/raw/main/images/Terraform.png)
## Installing Terraform
The easiest way to install Terraform is to use the package manager `Chocolatey`
### Installing Chocolatey
- Open Windows Powershell
- Run this command
```shell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```
For more information visit the [Chocolatey Website](https://chocolatey.org)
### Using Chocolatey to install Terraform
- To install Terraform run the following command in a terminal:
```shell
choco install terraform
```
For more information about Terraform installation, visit the [documentation](https://learn.hashicorp.com/tutorials/terraform/install-cli)
### Verifying the installation
Once Terraform is installed, it can be verified using this command to check the version installed:
```shell
terraform --version
```
Or using the `help` command for looking at the usable commands with Terraform
```shell
terraform -help
```
## Initialising Terraform
To initialise Terraform, the following command can be used:
```shell
terraform init
```
This will initialise Terraform by generating the `.terraform` folder and the `.terraform.lock.hcl` file
## Building the `main.tf` file
To tell Terraform to build an instance, the 'blueprints' of the instance need to be specified.

For this, a new file in the same directory needs to be made called `main.tf` where `.tf` is the Terraform extension.
### Specifying the provider
The first thing that needs to be read on the `main.tf` file is which provider is being used.
```terraform
# keyword: `provider`

# "aws" - Amazon Web Services
# "azure" - Microsoft Azure
# "gcp" - Google Cloud Platform

provider "aws" {
    # Specifying region
    region = "eu-west-1"
}
```
### Specifying the resources of the EC2 instance
Once the provider and region is specified, all the specific resources need to be defined.

Base resources that need to be defined include:
- The AMI used to build the EC2 instance
- The instance type

Because, the instance needs to be accessed via public SSH, creating with a public IP needs to defined as well as specifying the key name that the instance will be created with.

```terraform
resource "aws_instance" "app_instance" {

    ami = "ami-038d7b856fe7557b3"

    # Provide what type of instance that is being created
    instance_type = "t2.micro"

    # Allowing Public IP for the instance
    associate_public_ip_address = true

    # Specifying the SSH Key to be able to SSH into the instance
    key_name = "eng89_ray_key"
}
```
### Giving tags to the instance
The instance can also be given tags for easier identification.

The syntax used to define a tag is:
```terraform
name_of_tag = value_of_tag
```

For this instance, a name will be added so it can be found more easily.

**Note: This code block needs to be added within the resource code block**
```terraform
    # Giving the instance a name using tags
    # 'tags' is the keyword to list tags
    tags = {
        Name = "eng89_ray_terraform"
    }
```

## Testing the `main.tf` file for correct syntax
To test the syntax of the `main.tf` file, the following command can be used:
- Make sure this and following terraform commands are executed within the directory where the main.tf file is located

```shell
terraform plan
```
If the syntax is correct, terraform should throw no errors and the file is ready to be executed.

## Running the `main.tf` file to build an EC2 instance
To run the terraform file, the following command is used:
```shell
terraform apply
```
This prompts the user for a confirmation before building the instance to the requirements outlined in the terraform file.
- To confirm to terraform to build the EC2 instance- type `yes` followed by pressing `Enter`

## Destroying (Terminating) the EC2 instance using terraform
If the instance is no longer needed, it can be destroyed/terminated via terraform rather than using the AWS to delete it manually. This is an added benefit as it can be automated .e.g., after a certain duration.
To destroy the terraform-created EC2 instance, run the following command:
```shell
terraform destroy
```
- This prompts the same response as creating the instance to confirm that the user wants to destroy the instance.