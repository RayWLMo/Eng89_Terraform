# Creating a VPC

resource "aws_vpc" "eng89_ray_terraform_vpc" {
        cidr_block       = var.cidr_vpc
        instance_tenancy = "default"
        enable_dns_hostnames = true
        enable_dns_support = true

        tags = {
            Name = "eng89_ray_terraform_vpc"
        }
}

resource "aws_subnet" "eng89_ray_app_subnet" {
      vpc_id = aws_vpc.eng89_ray_terraform_vpc.id
      cidr_block = var.cidr_subnet
      map_public_ip_on_launch = true
      availability_zone = var.availability_zone
      tags = {
            Name = "eng89_ray_app_subnet"
      }      
}