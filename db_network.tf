resource "aws_security_group" "eng89_ray_terraform_SG_db" {
    vpc_id = aws_vpc.eng89_ray_terraform_vpc.id
    name = "eng89_ray_terraform_SG_db"
    description = "Security Group for the database server"

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = [var.my_ip]
    }

    ingress {
        from_port   = 27017
        to_port     = 27017
        protocol    = "tcp"
        cidr_blocks = ["10.206.1.0/24"]
    }

    ingress {
        from_port   = 3000
        to_port     = 3000
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_subnet" "eng89_ray_db_subnet" {
      vpc_id = aws_vpc.eng89_ray_terraform_vpc.id
      cidr_block = var.cidr_subnet_db
      map_public_ip_on_launch = true
      availability_zone = var.availability_zone
      tags = {
            Name = "eng89_ray_db_subnet"
      }      
}