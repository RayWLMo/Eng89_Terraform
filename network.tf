# Creating an Internet Gateway

resource "aws_internet_gateway" "eng89_ray_IG" {
      vpc_id = aws_vpc.eng89_ray_terraform_vpc.id

      tags = {
            Name = var.igw_name
      }
}

resource "aws_route_table" "eng89_ray_rt" {
    vpc_id = aws_vpc.eng89_ray_terraform_vpc.id
    
    route {
        cidr_block = "0.0.0.0/0" 
        gateway_id = aws_internet_gateway.eng89_ray_IG.id
    }
    
    tags = {
        Name = "eng89_ray_rt"
    }
}

resource "aws_route_table_association" "eng89_ray_rt_assoc" {
    subnet_id = aws_subnet.eng89_ray_app_subnet.id
    route_table_id = aws_route_table.eng89_ray_rt.id
}

resource "aws_security_group" "eng89_ray_terraform_SG" {
    vpc_id = aws_vpc.eng89_ray_terraform_vpc.id
    name = "eng89_ray_terraform_SG"
    description = "Security Group for the web app server"

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = [var.my_ip]
    }

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 3000
        to_port     = 3000
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

# resource "aws_security_group_rule" "allow_ssh" {
#     type              = "ingress"
#     from_port         = 22
#     to_port           = 22
#     protocol          = "tcp"
#     cidr_blocks       = ["0.0.0.0/0"]
#     security_group_id = aws_security_group.eng89_ray_terraform_SG.id
# }

resource "aws_network_acl" "eng89_ray_app_NACL" {
    vpc_id = aws_vpc.eng89_ray_terraform_vpc.id
    subnet_ids = [ aws_subnet.eng89_ray_app_subnet.id ]

    egress {
        protocol   = -1
        rule_no    = 100
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 0
        to_port    = 0
    }
    # egress {
    # 	protocol   = "tcp"
    # 	rule_no    = 110
    # 	action     = "allow"
    # 	cidr_block = "0.0.0.0/0"
    # 	from_port  = 443
    # 	to_port    = 443
    # }
  
    ingress {
        protocol   = -1
        rule_no    = 100
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 0
        to_port    = 0
    }

    # ingress {
    #     protocol   = "tcp"
    #     rule_no    = 110
    #     action     = "allow"
    #     cidr_block = "0.0.0.0/0"
    #     from_port  = 443
    #     to_port    = 443
    # }
    # ingress {
    #     protocol   = "tcp"
    #     rule_no    = 120
    #     action     = "allow"
    #     cidr_block = var.my_ip
    #     from_port  = 22
    #     to_port    = 22
    # }
  
  tags = {
    Name = "eng89_ray_app_NACL"
  }
}