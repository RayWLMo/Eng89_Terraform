# Building a script to connect to AWS and download/setup all dependencies required

# keyword: 'provider'
provider "aws" {
      # Launching an EC2 instance in EU-West-1
      region = "eu-west-1"
}

# keyword called "resource" provide resource name and give name with specific details to the service

# resource aws_ec2_instance, name it as eng89_ray_terraform, ami, type of instance, with or without IP
resource "aws_instance" "app_instance" {
      ami = var.instance_ami
      
      # Specifying the Subnet
      subnet_id = aws_subnet.eng89_ray_app_subnet.id

      # Provide what type of instance that is being created
      instance_type = var.instance_type

      # Allowing Public IP for the instance
      associate_public_ip_address = true

      # Specifying the SSH Key to Terraform
      key_name = var.aws_key_name

      # Specifying the Security Group
      vpc_security_group_ids = [aws_security_group.eng89_ray_terraform_SG.id]

      # Giving a proper name for the instance using tags
      # tags is the keyword to name the instance - eng89_ray_terraform
      tags = {
            Name = "eng89_ray_terraform_app"
      }
}